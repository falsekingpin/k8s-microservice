# k8s-microservice #

This repo contains some work around how to deploy a flask app to kubernetes(GKE). It features a simple hello-world app.

## Answers 
1. How long did it take to finish the test?
    - It took me between 4-5 hours to complete the entire test.

2. If you had more time, how could you improve your solution?
    - If I had more time, I would have improved the solution in different areas:
    - Automating the process of deployment from git commit on any deployment branch to the deployment, to do this I would have used Jenkins.
    - I deployed the applications using kubectl, a better way of deploying the applications in kubernetes would be using Helm Charts. This could make it easier for multiple microservice deployments. The advantage with this is, you get to manipulate different varaibles for your Application Infrastructure in a generic way, instead of modifying the deployment manifests.
    - I would have implemented a better NGINX config, right now I have it in one single nginx.conf, instead have a common conf for all the connf configurations and for different services have different confs in sites-enabled and include them in nginx.conf. This would make it easier for any change if we would have 100s of microservices and assuming we would do NGINX based routing.

3. What changes would you do to your solution to be deployed in a production environment?
    - Make the solution more structured, right now it is deployed with the help of a script. Instead of deploying it with the help of a script, it can be different Jenkins pipelines which can be used to abstract out the details.
    - Implementing Jenkins or any other CI and CD tool along with Nexus and Helm for building applications, storing artifacts and deploying applications in Kubernetes.
    - Handling errorneous an dedge cases while deployment. Making backup strategy for storing NGINX config in a bucket which can be synced with the actual deployed config.

4. Why did you choose this language over others?
    - I have worked with Terraform only on a learning basis, I implemented it in a solution for the first time. I have found terraform to be easy to use and along with it terraform has its advantages  of maintaining the state through a backend and also its declarative structure which is easy to read and also implement.
    - I used kubectl to deploy the application onto Kubernetes because its faster to implement and easier to setup. Also it has a wide set of utility commands to deploy and monitor the infrastructure.
    - I have implemented the solution using bash, as it is one of the widely used scripting languages across different systems. Also to implement something like Jenkins would take time and to implement the above solution in bash took a comparatively less time.
    - I have used python, flask and gunicorn to create the API and having it run on a server(gunicorn), as flask seemed the right framework to quickly build an API.

5. What was the most challenging part and why?
    - Implementing the Terraform, as I did not have much experience with terraform. Implementing it was a little challenging as it was relatively new for me. I didn't face any other challenges while implementing it.
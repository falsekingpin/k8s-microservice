#!/usr/bin/env python
"""main.py: main serves as entrypoint to the flask app."""
__author__      = "falsekingpin"
__copyright__   = "Copyright 2020, falsekinpin"

from flask import Flask
app = Flask(__name__)

@app.route("/")
def default_get():
    return "Welcome To Get"

@app.route("/hello")
def hello_get():
    response_json = {
        "hello" : "world"
    }
    return response_json

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)
variable "project" {
  type = "string"
  description = "Google Cloud project name"
}

variable "region" {
  type = "string"
  description = "Default Google Cloud region"
}

variable "cluster_name" {
  type = "string"
  description = "GKE Cluster name"
}

terraform {
  backend "gcs" {
    bucket = "bucket-id"
    prefix = "terraform"
    credentials = "account.json"
  }
}

provider "google" {
  credentials = "${file("account.json")}"
  project     = "${var.project}"
  region      = "${var.region}"
}

#!/usr/bin/env bash
# set -x
set -e

project_id=${1}
if [[ -z "${project_id// }" ]]
then
    exit
fi
echo "******Project ID********"
echo $project_id

bucket_name=${2:-"$(perl -le 'print map { (a..z,A..Z,0..9)[rand 62] } 0..9')"}
echo "******Bucket ID********"
echo $bucket_name

region_id=${3:-us-east1}
echo "******Region ID********"
echo $region_id

cluster_id=${4:k8s-microservice-test}
echo "******Cluster ID********"
echo $cluster_id

set_terraform_env(){

    echo "******Exporting Variables********"
    export BUCKET_ID=$bucket_name
    export ENVIRONMENT=production
    export PROJECT=$project_id

    echo "******Creating and Applying terraform conf********"
    gsutil versioning set on gs://${BUCKET_ID}

    echo "******Changed directory********"
    cd ..

    echo ""
    sed -i -e 's/bucket-id/'$bucket_name'/' infra-creation/google.tf

    echo "******Editing Production TFVars********"
    sed -i -e 's/project-id/'$project_id'/' infra-creation/production.tfvars
    sed -i -e 's/cluster-id/'$cluster_id'/' infra-creation/production.tfvars
    sed -i -e 's/region-id/'$region_id'/' infra-creation/production.tfvars

    echo "******Changed directory********"
    cd infra-creation
    terraform init
    terraform workspace new ${ENVIRONMENT}
    terraform init -var-file=${ENVIRONMENT}.tfvars
    terraform apply -var-file=${ENVIRONMENT}.tfvars
}

deploy_app(){
    
    echo "******Changed directory********"
    cd .. && cd kubernetes-config

    echo "******Connecting to K8s cluster********"
    gcloud container clusters get-credentials $cluster_id-cluster --region $region_id --project $project_id

    echo "******Creating Deployments********"
    kubectl create -f rest-app/deployment.yaml
    
    echo "******Deploying Service********"
    kubectl create -f rest-app/service.yaml

    echo "******Waiting for deployments to happen********"
    sleep 120

    echo "*****Storing Service IP*****"
    api_ip=$(kubectl get services --namespace default rest-app-svc --output jsonpath='{.status.loadBalancer.ingress[0].ip}')

    echo "*****Changing the API IP in NGINX*****"
    sed -i -e 's/api-ip/'$api_ip'/' nginx/nginx.conf

    echo "*****Creating MIME TYPES ConfigMap*****"
    kubectl create configmap mimeconf --from-file=nginx/mime.types --namespace default

    echo "*****Creating NGINX ConfigMap*****"
    kubectl create configmap nginxconf --from-file=nginx/nginx.conf --namespace default

    echo "*****Creating IndexHTML ConfigMap*****"
    kubectl create configmap indexhtmlconf --from-file=nginx/html/index.html --namespace default

    echo "*****Creating 50xHTML ConfigMap*****"
    kubectl create configmap 50xhtmlconf --from-file=nginx/html/50x.html --namespace default

    echo "*****Deploying NGINX Deployment*****"
    kubectl create -f nginx/nginx-deployment.yaml

    echo "*****Deploying NGINX SVC*****"
    kubectl create -f nginx/nginx-svc.yaml

    echo "******Waiting for deployments to happen********"
    sleep 120

    echo "*****Storing the NGINX IP*****"
    nginx_ip=$(kubectl get services --namespace default nginx --output jsonpath='{.status.loadBalancer.ingress[0].ip}')

    echo "************************************************************"
    echo "You can check the NGINX Service Status at : $nginx_ip"
    echo "************************************************************"

    echo "*****GET request at API*****"
    curl http://$nginx_ip/hello

    echo "*****Finished creating cluster and deploying the app*****"

}

set_terraform_env
deploy_app
# k8s-microservice #

This repo contains some work around how to deploy a flask app to kubernetes(GKE). It features a simple hello-world app.

## Prerequisite 

1. To run the deployment script for creation of cluster and deployment of app, one needs to have a Google Cloud Account, if you don't have you can create one at : [How to create a GCP Account](console.cloud.google.com)

2. After having created a GCP Account you will need to enable Kubernetes Engine API, just go to Kubernetes Engine API by typing Kubernetes Engine in the search bar and click on enable API. Post the API has been enabled, you will be able to create a Kubernetes Cluster

3. Now we will need to create a Service Account in order for us to be able to create infrastructure and deploy it in GCP. Head Over to IAM by typing IAM in the search bar, go to service account and create a service account with role editor. After having created the service account download the service account key in json format. Stuck? Follow it here : [A reference blog to create service account](https://www.codepoc.io/blog/gcp/5993/how-to-create-service-account-in-google-cloud-and-download-private-key-json-file)

4. After creating the service account, head over to create a bucket. Why we need it? Terraform stores its state files in this bucket. Go to storage in GCP and create a bucket with the name you want. [A reference doc for creating buckets](https://cloud.google.com/storage/docs/creating-buckets)

5. After having downloaded the service account json and cloned the repo, now move the service account json to the directory **infra-creation** with the name as account.json. Post that check the permissions for the file, since its a json key the owner running the script should be the owner of the file , something like this :
*-rw-r--r--@  1 falsekingpin  k8stest  2341 Apr 11 19:43 account.json*

6. Now clone the repo using :
    ```sh
    git clone https://falsekingpin@bitbucket.org/falsekingpin/k8s-microservice.git
    ```

### Installation Requirements
Installing Terraform, GCloud SDK and kubectl utility on the machine that will run the deployment script.

1. If you don't have terraform installed, you cann follow this guide to install terraform :
[Guide to install terraform](https://learn.hashicorp.com/terraform/getting-started/install.html)

2. We also need GCloud SDK setup to access the GCP, if you dont have setup you can follow the guide here :
[Guide to install gcloud](https://cloud.google.com/sdk/install). After installing the gcloud sdk you need to configure it for using with some default presets. Basically you need to type in *gcloud init* , it will show you a list of options, choose to *Re-initialize this configuration [default] with new setting*. After that it will ask you to login with an gcp account, login with the account you created GCP account in Prerequistes step-1. Post that it will ask to select default regions and other things, that can be left as default. Stuck? You can refere this blog : [Initialize GCloud](https://cloud.google.com/sdk/docs/initializing)

3. Post having initialized GCloud sdk, we need the last utility to be setup i.e kubectl. We are using kubectl utility for deploying API and NGINX to the GKE Cluster. To setup kubectl, follow this guide : [kubectl setup](https://kubernetes.io/docs/tasks/tools/install-kubectl/)

With this we are done, creating a GCP account, and setting up our workspace for deploying apps into kubernetes.

### How to RUN?

1. Now that we have setup the environment, lets get started on how to run the project. It is very simple, head to the directory **creation-script** from the repo we cloned. Run the following command.

    ```sh
    bash infra-creation-script.sh project-id bucket-id region-id cluster-id
    ```

- During this process the script will ask a prompt whether to create the infrastructure or not? Reply the prompt with **yes**

- project-id is the unique id for the GCP project in which we want to create a GKE cluster and deploy the APP. You can get the project-id from the GCP console.
- bucket-id is the name of the bucket we created in Prerequsite step-4
- region-id is the region in which we want to deploy the GKE cluster in. ex. us-east1
- cluster-id is the name you want to give your cluster

The output of the command should be something like this :
```sh
    ******Project ID********
    project-id
    ******Bucket ID********
    bucket-name
    ******Region ID********
    region-name
    ******Cluster ID********
    cluster-name
    ******Exporting Variables********
    ******Creating and Applying terraform conf********
    Enabling versioning for gs://bucket-id/...
    ******Changed directory********
    ...........
    ...........
    ...........
    ************************************************************
    You can check the NGINX Service Status at : NGINX Service IP
    ************************************************************
    *****GET request at API*****
    {"hello":"world"}
    *****Finished creating cluster and deploying the app*****
```

Post creating the app, if you would want to destroy the cluster, it can be done simply by the following command executed from **infra-creation** directory
    
    ```sh
    terraform destroy -auto-approve
    ```
- The following command will start the process of destroying the infrastructure, it will ask for a few prompts listed below:
    - project-id : project-id is the unique id for the GCP project in which we want to create a GKE cluster and deploy the APP. You can get the project-id from the GCP console.
    - machine-type : machine-type is the machine type we choose for our GCP cluster, by default it is n1-standard-1
    - region-id : region-id is the region in which we want to deploy the GKE cluster in. ex. us-east1
    - cluster-id : cluster-id is the name you want to give your cluster

If you want to tickle around with the python Flask app, which is very staright forward if you know python.You can in the directory **rest-app**. Also included is a doccker file. For setting up a virtual env you can do the following :

    ```sh
    python3 -m venv k8s-env   ##create a environment
    source k8s-env/bin/activate ##activate the virtual env
    pip install -r requirements.txt
    ```

### Rationale behind the solution

1. I chose to go with docker and kubernetes for making the solution platform agnostic, so it can be deployed on any type of compute whether its AWS, GCP or On-Prem.

2. I have used Terraform for creating the infrastructure, since it has a diverse set of modules around different cloud providers and also its declarative syntax for defining infrastructure is very straightforward. Also it provides us easy way for defining workspaces and then destroying any infrastructure we created. Also it maintains the state files which is a commendable feature of Terraform.

3. For deploying applications on Kubernetes I have used kubectl, as its the default command line utility to interact 
with Kubernetes Clusters and is also faster to implement than other utilities.

4. I have used Kubernetes manifests to define my Application Infrastructure, it is clean and requires less time to define and have it deployed onto the kubernetes infrastructure.

5. Why I went with Kubernetes? It helps us use infrastructure in a optimal manner. Makes Application platform agnostic. Helps with faster deployment times and spin up times. Also helps with scaling is some of the cases, where we can predict the load and have the infrastructure already setup.

6. Docker? Its the default container techonlogy I think all of us use and has a wide support amongst the community also. Plus Docker also gives us advantage around implementation.


